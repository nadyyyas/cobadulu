from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class toko (models.Model):
    nama_toko = models.CharField(max_length = 30)
    nama_pemilik = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    alamat = models.TextField()

    def __str__(self):
        return self.nama_toko

class produk (models.Model):
    nama_produk = models.CharField(max_length = 30)
    harga_produk = models.CharField(max_length = 30)
    keterangan_produk = models.CharField(max_length = 30)
    nama_pemilik = models.ForeignKey(toko, on_delete=models.CASCADE, default=1)
    gambar_produk = models.ImageField()

    def __str__(self):
        return self.nama_produk
        
